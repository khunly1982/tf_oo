<?php

// Permet de charger l'ensemble des class dont le namespace commence par App
require_once __DIR__. "./vendor/autoload.php";

// Permet la récupération de l'url du navigateur
$url = $_SERVER['REQUEST_URI']; // => exemple: $url = '/user/list'

// Défini le nom complet du controller par défaut => App\Controller\HomeController
$ctrlName = "App\\Controller\\". ucfirst("home"). "Controller";
// Défini le nom de l'action du controller par défaut => index();
$ctrlAction = "index";

// Je traites l'url pour obtenir dans un tableau chaque partie de l'url
// url => /home/index => ['', 'home', 'index']
/** @var array $urlSplitted */
$urlSplitted = preg_split("/\//", $url);

// Defini le dossier de vue par défault
$name = "Home";

// Vérifie si il existe une url du type /XXXXX
if (sizeof($urlSplitted) >= 2 && $urlSplitted[1] !== "") {
    // Modifie le nom du controller en fonction de l'url
    $ctrlName = "App\\Controller\\". ucfirst($urlSplitted[1]). "Controller";
    // $ctrlName = App\Controller\XxxxxController

    // Modifie le fichier de view a utiliser
    $name = ucfirst($urlSplitted[1]);
}

//Verifie qu'il existe une action qui est définie dans l'url
if (sizeof($urlSplitted) >= 3 && $urlSplitted[2] !== "") {
    // Modifie
    $ctrlAction = $urlSplitted[2];
}
$id = null;
//verifie si il existe un id dans l'url
// ['', 'user', 'display', 1]
if (sizeof($urlSplitted) >= 4 && $urlSplitted[3] != null) {
    $id = $urlSplitted[3];
}

//$ex = new \App\Controller\HomeController();
// J'instancie une nouvelle classe du controller
$ctrl = new $ctrlName();

//var_dump($ctrl->$ctrlAction());
// J'execute l'action du controller
/** @var \Lib\View\View $view */
$view = $id !== null ? $ctrl->$ctrlAction($id) : $ctrl->$ctrlAction();

// J'extrait d'un tableau associatif toutes les variables définie
// [ key1 => value1, key2 => value2]
// key => le nom de la variable
// value => la valeur de la variable
extract($view->getVars());

// Permet la création d'un template avec des variables définies dans le tableau de la view extrait précédement
//ob_start();
require __DIR__. "./src/View/". $name. "/". $view->getName(). ".php";
//echo ob_get_clean();


<?php

namespace App\Repository;

use Exception;
use App\Model\TypeCuisine;

class TypeCuisineRepository extends BaseRepository 
{
    public function getAll() 
    {
        $query = "SELECT * FROM TypeCuisine";
        $stmt = $this->pdo->prepare($query);

        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_CLASS, TypeCuisine::class);
        if($stmt->errorInfo()[2]) {
            throw new Exception();
        }
        return $result; 
    }
}
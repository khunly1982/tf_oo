<?php


namespace App\Repository;


use App\Model\Adresse;
use App\Model\Restaurant;
use App\Model\TypeCuisine;
use Exception;

class RestaurantRepository extends BaseRepository
{
    public function getAll() {
        /** @var \PDOStatement $stmt */
        $stmt = $this->pdo->prepare(
            "SELECT r.*, t.nom typeCuisineNom 
            FROM restaurant r 
            JOIN typeCuisine t ON t.id = r.typeCuisineId"
        );

        $stmt->execute();

        /** @var array $res */
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $data = [];

        foreach ($res as $resto) {
            $restaurant = new Restaurant();
            $restaurant->setId($resto["id"]);
            $restaurant->setName($resto["nom"]);

            $type = new TypeCuisine();
            $type->setId($resto["typeCuisineId"]);
            $type->setNom($resto["typeCuisineNom"]);
            $restaurant->setTypeCuisine($type);

            $address = new Adresse();
            $address->setRue($resto["adresseRue"]);
            $address->setVille($resto["adresseVille"]);
            $address->setCp($resto["adresseCp"]);
            $address->setNum($resto["adresseNum"]);

            $restaurant->setAdresse($address);

            $data[] = $restaurant;
        }

        return $data;
    }

    public function insert($post) {
        $query = "INSERT INTO Restaurant(nom,adresseRue,adresseNum,adresseCp,adresseVille,typeCuisineId)
                  VALUES(?,?,?,?,?,?)";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([
            $post['nom'], 
            $post['adresseRue'],
            $post['adresseNum'],
            $post['adresseCp'],
            $post['adresseVille'],
            $post['typeCuisineId']
        ]);
        if($stmt->errorInfo()[2]) {
            throw new Exception();
        }
    }
}
<?php

namespace App\Repository;

abstract class BaseRepository {
    /** @var \PDO $pdo */
    protected $pdo;

    public function __construct() {
        $this->pdo = new \PDO("mysql:host=localhost;port=3306;dbname=demo_php", "root", null);
    }
}
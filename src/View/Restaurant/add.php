<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- $_POST <=> [key => valeur, ...] -->
    <form method="post">
        <div>
            <label for="nom">Nom du restaurant</label>
            <input id="nom" type="text" name="nom">
        </div>
        <div>
            <label for="adresseRue">Rue du restaurant</label>
            <input id="adresseRue" type="text" name="adresseRue">
        </div>
        <div>
            <label for="adresseNum">Numéro du restaurant</label>
            <input id="adresseNum" type="text" name="adresseNum">
        </div>
        <div>
            <label for="adresseCp">Code postal du restaurant</label>
            <input id="adresseCp" type="text" name="adresseCp">
        </div>
        <div>
            <label for="adresseVille">Ville du restaurant</label>
            <input id="adresseVille" type="text" name="adresseVille">
        </div>
        <div>
            <label for="typeCuisineId">Type de cuisine</label>
            <select id="typeCuisineId" name="typeCuisineId">
                <?php foreach($types as $type){ ?>
                    <option value="<?= $type->getId() ?>"><?= $type->getNom() ?></option>
                <?php } ?>
            </select>
        </div>
        <div>
            <button>Envoyer</button>
            <!-- <input type="submit" value="Envoyer"/> -->
        </div>
    </form>
</body>
</html>
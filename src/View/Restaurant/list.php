<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Nom</th>
                <td>Rue</td>
                <td>Num</td>
                <td>Type de cuisine</td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($restaurants as $resto) { ?>
            <tr>
                <td><?= $resto->getName() ?></td>
                <td><?= $resto->getAdresse()->getRue() ?></td>
                <td><?= $resto->getAdresse()->getNum() ?></td>
                <td><?= $resto->getTypeCuisine()->getNom() ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</body>
</html>
<?php


namespace App\Controller;


use App\Repository\RestaurantRepository;
use Lib\View\View;
use App\Model\Restaurant;
use App\Model\Adresse;
use App\Model\TypeCuisine;
use App\Repository\TypeCuisineRepository;
use Exception;

class RestaurantController
{
    /** @var RestaurantRepository $restaurantRepository */
    private $restaurantRepository;
    private $typeCuisineRepo;

    public function __construct()
    {
        $this->restaurantRepository = new RestaurantRepository();
        $this->typeCuisineRepo = new TypeCuisineRepository();
    }

    public function list() {
        return new View("list", [
            "restaurants" => $this->restaurantRepository->getAll()
        ]);
    }

    public function add() {
        // if get
        if($_SERVER['REQUEST_METHOD'] === 'GET') {
            
            // afficher le formulaire
            return new View('add', [
                'types' => $this->typeCuisineRepo->getAll()
            ]);
        }
        // else if post traitement du formulaire
        else { // POST
            // transformer le tableau en objet
            try {
                $restaurant = new Restaurant();
                $restaurant->setName($_POST["nom"]);

                $type = new TypeCuisine();
                $type->setId($_POST["typeCuisineId"]);
                $restaurant->setTypeCuisine($type);
                
                $address = new Adresse();
                $address->setRue($_POST["adresseRue"]);
                $address->setVille($_POST["adresseVille"]);
                $address->setCp($_POST["adresseCp"]);
                $address->setNum($_POST["adresseNum"]);
                
                $restaurant->setAdresse($address);
                // validation des données
                $this->restaurantRepository->insert($_POST);

                // rediriger
                header('location:http://localhost:8000/restaurant/list');
            }
            catch(Exception $e) {
                echo 'Une erreur est survenue';
                return new View('add', [
                    //TODO LATER (liste des type de cuisine)
                ]);
            }
        }
        
    }
}
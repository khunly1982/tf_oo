<?php


namespace App\Model;

use Exception;

class Restaurant
{
    /** @var int îd */
    private $id;
    /** @var string $name */
    private $name;
    /** @var Adresse $adresse */
    private $adresse;

    private $typeCuisine;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Restaurant
     */
    public function setId(int $id): Restaurant
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Restaurant
     */
    public function setName(string $name): Restaurant
    {
        if(empty($name)) {
            // erreur
            throw new Exception();
        }
        if(strlen($name) > 255) {
            // erreur
            throw new Exception();
        }
        $this->name = $name;
        return $this;
    }

    /**
     * @return Adresse
     */
    public function getAdresse(): Adresse
    {
        return $this->adresse;
    }

    /**
     * @param Adresse $adresse
     * @return Restaurant
     */
    public function setAdresse(Adresse $adresse): Restaurant
    {
        $this->adresse = $adresse;
        return $this;
    }



    /**
     * Get the value of typeCuisine
     */ 
    public function getTypeCuisine()
    {
        return $this->typeCuisine;
    }

    /**
     * Set the value of typeCuisine
     *
     * @return  self
     */ 
    public function setTypeCuisine($typeCuisine)
    {
        $this->typeCuisine = $typeCuisine;

        return $this;
    }
}
<?php


namespace App\Model;

use Exception;

class Adresse
{
    private $rue;
    private $ville;
    private $cp;
    private $num;

    /**
     * @return mixed
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * @param mixed $rue
     */
    public function setRue($rue): void
    {
        if(empty($rue)) {
            throw new Exception();
        }
        if (strlen($rue) > 255)
        {
            throw new Exception();
        }
        $this->rue = $rue;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville): void
    {
        if(empty($ville)) {
            throw new Exception();
        }
        if(strlen($ville) > 100) {
            throw new Exception();
        }
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp): void
    {
        if(empty($cp)) {
            throw new Exception();
        }
        if(strlen($cp) != 4 || !filter_var($cp, FILTER_VALIDATE_INT)) {
            throw new Exception();
        }
        $this->cp = $cp;
    }

    /**
     * @return mixed
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * @param mixed $num
     */
    public function setNum($num): void
    {
        if(!empty($num) && strlen($num) > 10) {
            throw new Exception();
        }
        $this->num = $num;
    }


}